﻿using System;
using System.Collections.Generic;

// See also: https://refactoring.guru/design-patterns/observer/csharp/example

namespace observer
{
    class Program
    {
        static void Main(string[] args)
        {
            // Client application
            CConcreteSubject subject1 = new CConcreteSubject(); // This removes the benefit of the interface but we
            CConcreteSubject subject2 = new CConcreteSubject(); // can't do it better in school.
            CObserver observer1 = new CFirstConcreteObserver(subject1);
            CObserver observer2 = new CFirstConcreteObserver(subject2);
            // Attach
            subject1.Attach(observer1);
            subject1.Attach(observer2);
            subject2.Attach(observer1);
            subject2.Attach(observer2);
            // Do stuff
            subject1.SubjectState = "Hello";
            subject2.SubjectState = "World";
            // Detach
            subject1.Detach(observer1);
            subject1.Detach(observer2);
            subject2.Detach(observer1);
            subject2.Detach(observer2);
        }
    }

    interface CSubject
    {
        public void Attach(CObserver observer);
        public void Detach(CObserver observer);
        public void Notifiy();
    }

    interface CObserver
    {
        public void Update();
    }

    class CConcreteSubject : CSubject
    {
        private List<CObserver> _observers;
        private string _subjectState;

        public string SubjectState
        {
            get { return _subjectState; }
            set
            {
                _subjectState = value;
                Console.WriteLine("CConcreteSubject ({0}): Update value of state to: {1}",
                    GetHashCode(),
                    value);
                Notifiy();
            }
        }

        public CConcreteSubject()
        {
            _observers = new List<CObserver>();
            _subjectState = "";
        }

        public void Attach(CObserver observer)
        {
            Console.WriteLine("CConcreteSubject ({0}): Observer {1} was added to the list of observers.", 
                GetHashCode(),
                observer.GetHashCode());
            _observers.Add(observer);
        }

        public void Detach(CObserver observer)
        {
            Console.WriteLine("CConcreteSubject ({1}): Observer {0} was removed to the list of observers.",
                observer.GetHashCode(),
                GetHashCode());
            _observers.Remove(observer);
        }

        public void Notifiy()
        {
            Console.WriteLine("CConcreteSubject ({1}): Starting to notify observers with new value: {0}",
                _subjectState,
                GetHashCode());
            foreach (CObserver currentObserver in _observers)
            {
                Console.WriteLine("CConcreteSubject ({1}): Observer {0} was notified.",
                    currentObserver.GetHashCode(),
                    GetHashCode());
                currentObserver.Update();
            }

            Console.WriteLine("CConcreteSubject ({1}): Ended to notifying observers with new value: {0}",
                _subjectState,
                GetHashCode());
        }
    }

    class CFirstConcreteObserver : CObserver
    {
        // FIXME: In school we don't learn this but since we don't know about generics and other fancy stuff we need
        //        to use it like this.
        private CConcreteSubject subjectToWatch;

        public CFirstConcreteObserver(CConcreteSubject subjectToWatch)
        {
            this.subjectToWatch = subjectToWatch;
        }

        public void Update()
        {
            Console.WriteLine("CFirstConcreteObserver ({2}): Update from ConcreteSubject (instance: {0}): {1}",
                subjectToWatch.GetHashCode(),
                subjectToWatch.SubjectState,
                GetHashCode());
        }
    }
}